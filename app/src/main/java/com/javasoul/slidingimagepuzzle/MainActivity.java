package com.javasoul.slidingimagepuzzle;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.picasso.transformations.CropSquareTransformation;

public class MainActivity extends AppCompatActivity {

    private GridLayout gridLayout;

    private ImageView img1;
    private ImageView img2;
    private ImageView img3;
    private ImageView img4;
    private ImageView img5;
    private ImageView img6;
    private ImageView img7;
    private ImageView img8;
    private ImageView img9;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(this);

        gridLayout = findViewById(R.id.grid_layout);
        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);
        img5 = findViewById(R.id.img5);
        img6 = findViewById(R.id.img6);
        img7 = findViewById(R.id.img7);
        img8 = findViewById(R.id.img8);
        img9 = findViewById(R.id.img9);

        final Target target = new Target(){
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Log.e("Satria","Load success");
                Bitmap[] bitmaps = splitBitmap(bitmap);
                img1.setImageBitmap(bitmaps[0]);
                img2.setImageBitmap(bitmaps[1]);
                img3.setImageBitmap(bitmaps[2]);
                img4.setImageBitmap(bitmaps[3]);
                img5.setImageBitmap(bitmaps[4]);
                img6.setImageBitmap(bitmaps[5]);
                img7.setImageBitmap(bitmaps[6]);
                img8.setImageBitmap(bitmaps[7]);
                //img9.setImageBitmap(bitmaps[8]);

                progressDialog.dismiss();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Log.e("Satria","Load failed");
                progressDialog.dismiss();
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Loading image...");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        };

        Picasso.with(this).load("http://www.satriawinarah.com/temp/puzzle.jpg").
                transform(new CropSquareTransformation()).into(target);

        img1.setTag(target);

        onGridTouch();
    }

    public Bitmap[] splitBitmap(Bitmap picture) {
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(picture, 300, 300, true);
        Bitmap[] imgs = new Bitmap[9];
        imgs[0] = Bitmap.createBitmap(scaledBitmap, 0, 0, 80 , 80);
        imgs[1] = Bitmap.createBitmap(scaledBitmap, 80, 0, 80, 80);
        imgs[2] = Bitmap.createBitmap(scaledBitmap,160, 0, 80,80);
        imgs[3] = Bitmap.createBitmap(scaledBitmap, 0, 80, 80, 80);
        imgs[4] = Bitmap.createBitmap(scaledBitmap, 80, 80, 80,80);
        imgs[5] = Bitmap.createBitmap(scaledBitmap, 160, 80,80,80);
        imgs[6] = Bitmap.createBitmap(scaledBitmap, 0, 160, 80,80);
        imgs[7] = Bitmap.createBitmap(scaledBitmap, 80, 160,80,80);
        imgs[8] = Bitmap.createBitmap(scaledBitmap, 160,160,80,80);
        return imgs;
    }

    private void onGridTouch() {
        for(int i=0; i<gridLayout.getChildCount(); i++) {
            ImageView imgView = (ImageView) gridLayout.getChildAt(i);
            imgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    moveImage((ImageView) v);
                }
            });
        }
    }

    private void moveImage(ImageView imgView) {
        if(!imgView.getTag().equals("empty")) {
            ImageView emptyImageView = findEmptyImageView(imgView);
            if(emptyImageView != null) {
                emptyImageView.setImageDrawable(imgView.getDrawable());
                emptyImageView.setTag("filled");

                imgView.setImageDrawable(getResources().getDrawable(R.drawable.empty));
                imgView.setTag("empty");
            }
        }
    }

    private ImageView findEmptyImageView(ImageView imageView) {
        for(int i=0; i<gridLayout.getChildCount(); i++) {
            ImageView imgView = (ImageView) gridLayout.getChildAt(i);
            if(imgView == imageView) {
                List<Integer> siblings = findSibling(i);

                for(Integer sib: siblings) {
                    ImageView siblingView = (ImageView) gridLayout.getChildAt(sib);
                    if(siblingView.getTag().equals("empty")) {
                        return siblingView;
                    }
                }
            }
        }
        return null;
    }

    private List<Integer> findSibling(int currentPosition) {
        int[][] array = {{0,1,2},{3,4,5},{6,7,8}};
        List<Integer> siblings = new ArrayList<>();

        for(int i=0; i<array.length; i++) {
            for(int j=0; j<array[i].length; j++) {
                if(array[i][j] == currentPosition) {
                    int[] up = {i-1, j};
                    int[] down = {i+1, j};
                    int[] left = {i, j-1};
                    int[] right = {i, j+1};

                    if(checkIndexOutOfBound(up, array)) {
                        siblings.add(array[up[0]][up[1]]);
                    }

                    if(checkIndexOutOfBound(down, array)) {
                        siblings.add(array[down[0]][down[1]]);
                    }

                    if(checkIndexOutOfBound(left, array)) {
                        siblings.add(array[left[0]][left[1]]);
                    }

                    if(checkIndexOutOfBound(right, array)) {
                        siblings.add(array[right[0]][right[1]]);
                    }
                }
            }
        }

        return siblings;
    }

    private boolean checkIndexOutOfBound(int[] index, int[][] array) {
        try {
            int result = array[index[0]][index[1]];
            return true;
        } catch(IndexOutOfBoundsException e) {
            return false;
        }
    }

}
